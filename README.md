# 調整-Firebase

## これはなに？

ハンズオン学習用に用意されたスケジュール調整アプリケーションです。
[調整さん](https://chouseisan.com/)のスペックを模倣しています。

## 事前準備

ハンズオンに先立って、事前準備をお願いします。

- [インストールとサインアップ](./docs/env/install-and-signup.md)

## 導入説明

作業前に、当日の流れ、進め方を説明します。

## ハンズオン作業

- [プロジェクトの準備をする](docs/hands-on/00_前準備.md)
- [イベントを登録する](docs/hands-on/01_イベントを登録する.md)
- [イベント詳細を表示する](docs/hands-on/02_イベント詳細を表示する.md) 
- [Firebaseで手動デプロイ](docs/hands-on/03_Firebaseで手動デプロイ.md) 
- [GitLabのCI/CDを設定する](docs/hands-on/04_GitLab_CICDを設定.md)

