import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyAhVJGwm-F3QeErBWakcOzKXM6E4aOXda8",
  authDomain: "chousei-firebase-84730.firebaseapp.com",
  databaseURL: "https://chousei-firebase-84730.firebaseio.com",
  projectId: "chousei-firebase-84730",
  storageBucket: "",
  messagingSenderId: "364376606301",
  appId: "1:364376606301:web:488fd9e5afd1124c7defb4"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
